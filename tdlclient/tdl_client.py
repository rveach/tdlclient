#! /usr/bin/env python

import configparser
import datetime
import getpass
import json
import logging
import paramiko
import os
import shutil
import subprocess
import tempfile
import traceback
import yaml

from pathlib import Path
from typing import Optional

from tdlclient.file_obs import CompletedDownload
from tdlclient.hashing import sha256_checksum

class TdlClient:

    def __init__(
        self,
        config_file_path: Optional[str] = "/etc/tdl-client-config.yaml",
    ):

        self.log = logging.getLogger(__name__)

        self.log.debug("Using config file %s", config_file_path)

        if config_file_path and os.path.exists(config_file_path):
            with open(config_file_path, "r") as f:
                config = yaml.safe_load(f)
        else:
            self.log.warning("Unable to find config at path %s, using empty config")
            config = {}


        # init-ing ssh paramiko clients
        self.ssh_client = paramiko.client.SSHClient()
        self.ssh_client.load_system_host_keys()
        self.sftp_client = None # placeholder for now

        remote_host = config.get('remote_host')
        remote_user = config.get('remote_user', getpass.getuser())

        self.log.debug("Connectiong to host %s as user %s", remote_host, remote_user)

        # define ssh connection params
        if config.get('ssh_auto_add_hosts', False):
            self.ssh_client.set_missing_host_key_policy(paramiko.client.AutoAddPolicy)
        self.connect_params = {
            "remote_host": remote_host,
            "remote_port": int(config.get('remote_port', 22)),
            "remote_user": remote_user,
            "password": config.get("ssh_password", None),
            "look_for_keys": config.get("ssh_look_for_keys", True),
            "timeout": int(config.get("ssh_timeout", 5)),
        }

        # loading ssh keys
        pkey_path = config.get("ssh_pkey_path")
        if os.path.isfile(pkey_path):
            self.log.info("Loading private key at: {}".format(pkey_path))
            pkey = paramiko.RSAKey.from_private_key_file(
                pkey_path,
                password=config.get("pkey_password", None),
            )
            self.connect_params['pkey'] = pkey
        else:
            self.log.info("Not loading a private key - not found.")
            self.connect_params['pkey'] = None

        # setting temp directory where downloads in progress are stored.
        self.temp_dir = config.get("temp_dir", "/tmp")
        self.log.debug("Using temporary directory: {}".format(self.temp_dir))

        # set the path of unrarall
        unrarall_exe = config.get("unrarall_exe", shutil.which("unrarall"))
        if not unrarall_exe:
            self.log.error("unrarall executable is not set or not found in path. Downloads will not be extracted")
            self.unrarall_exe = None
        elif not os.path.isfile:
            self.log.error("unrarall executable not found at {}. Downloads will not be extracted.".format(unrarall_exe))
            self.unrarall_exe = None
        else:
            self.log.info("Using unrarall found at {}".format(unrarall_exe))
            self.unrarall_exe = unrarall_exe
            # warn if cksfv is not found
            if not shutil.which("cksfv"):
                self.log.warning("cksfv not found.  SFV files will not be validated before unrarring.")
            else:
                self.log.debug("cksfv executable: {}".format(shutil.which("cksfv")))

        # set chown user and group
        chown_user = config.get("chown_user", None)
        chown_group = config.get("chown_group", None)
        if chown_user:
            self.chown_user = chown_user

            if chown_group:
                self.chown_group = chown_group
            else:
                self.chown_group = chown_user
        else:
            self.chown_user = None
            self.chown_group = None
        self.log.debug("chown user: {}".format(self.chown_user))
        self.log.debug("chown group: {}".format(self.chown_group))

        # set extensions to delete
        self.delete_exts = config.get("delete_exts", ['sfv', 'rar'])
        self.log.debug("Extensions to delete: {}".format(", ".join(self.delete_exts)))

        # set default download dir
        self.default_dl_loc = config.get("default_destination")
        os.makedirs(self.default_dl_loc, exist_ok=True)
        self.__chown(self.default_dl_loc)
        self.log.debug("Default download location: {}".format(self.default_dl_loc))

        # set other download dirs
        self.download_folders = config.get("download_folders", {})
        for key in self.download_folders.keys():
            os.makedirs(self.download_folders[key], exist_ok=True)
            self.__chown(self.download_folders[key])
            self.log.debug("Items from {s} will be moved to {d}".format(
                s=key,
                d=self.download_folders[key],
            ))

        self.incompl_file_ext = config.get("incomplete_file_ext", ".new")
        self.log.debug("Incomplete json file extension: {}".format(self.incompl_file_ext))
        self.compl_file_ext = config.get("completed_file_ext", ".done")
        self.log.debug("Complete json file extension: {}".format(self.compl_file_ext))

    def __chown(self, chown_path):
        """ abstract chowning """

        if self.chown_user:
            self.log.debug("Chowning {f} to {u}:{g}".format(
                f=chown_path,
                u=self.chown_user,
                g=self.chown_group,
            ))
            shutil.chown(chown_path, user=self.chown_user, group=self.chown_group)

        else:
            self.log.info("Not chowning {}".format(chown_path))

    def __connect_to_remote(self):
        
        # connect
        self.ssh_client.connect(
            self.connect_params['remote_host'],
            port=self.connect_params['remote_port'],
            username=self.connect_params['remote_user'],
            pkey=self.connect_params['pkey'],
            timeout=self.connect_params['timeout'],
            look_for_keys=self.connect_params['look_for_keys'],
        )
        self.sftp_client = self.ssh_client.open_sftp()

    def __close_connection(self):
        if self.sftp_client:
            self.sftp_client.close()
        self.ssh_client.close()

    def __read_file(self, remote_file_path, parse_json=False, parse_ini=False):
        """ reads a remote file and returns the contents """

        # open the remote file
        remote_fd =  self.sftp_client.open(remote_file_path, mode='r')

        # optionally, parse ini
        if parse_ini:
            contents = configparser.ConfigParser()
            contents.read_file(remote_fd)

        # optionally, parse json
        elif parse_json:
            contents = json.load(remote_fd)

        # fallback and just read it.
        else:
            contents = remote_fd.read()

        # close our remote file and return
        remote_fd.close()
        return contents

    def __mark_json_done(self, remote_jfile):
        """ rename a remote file """

        done_path = remote_jfile.replace(self.incompl_file_ext, self.compl_file_ext)

        self.sftp_client.posix_rename(remote_jfile, done_path)
        self.log.info("Renamed remote json file from {s} to {d}".format(
            s=remote_jfile,
            d=done_path,
        ))

    def __download_file(self, remote_file_path, local_file_path, checksum=None, retries=3):
        """ download a single file, checksum results """

        self.log.info(" - Downloading {s} to {d}".format(
            s=remote_file_path,
            d=local_file_path,
        ))

        download_success = False
        my_retries = 0

        while not download_success:

            if my_retries > retries:
                self.log.error("Exhausted retries!!")
                raise RuntimeError("There have been too many attempts to download {}".format(remote_file_path))

            try:
                self.sftp_client.get(remote_file_path, local_file_path)
            except Exception as e:
                self.log.warning(traceback.format_exc())
                my_retries += 1

            if not os.path.exists(local_file_path):
                # the file does not exist, something failed.
                my_retries += 1

            # do a checksum if one was passed.
            elif checksum:
                # if the checksum does not match, delete and try again.
                if checksum != sha256_checksum(local_file_path):
                    os.remove(local_file_path)
                    my_retries += 1
                    self.log.warning("Checksum failed!")
                else:
                    # checksum matches, success
                    download_success = True
                    self.log.info(" + Download/Checksum Success!")

            else:
                download_success = True
                self.log.info(" + Download Success!")

    def __queue_json_file(self, remote_file):
        """ read json data, which gets queued for download elsewhere """
        remote_json_path = os.path.join(self.remote_directory, remote_file.filename)
        file_data = self.__read_file(remote_json_path, parse_json=True)
        return CompletedDownload(file_data, remote_json_path)

    def __unrarall(self, unrar_dir):
        """ Run unrarall in the directory """

        unrarall_cmd = [self.unrarall_exe, "--clean=all", "."]
        unrarall_res = subprocess.run(unrarall_cmd, cwd=unrar_dir, timeout=600, capture_output=True)

        if unrarall_res.returncode != 0:
            self.log.warning("Unrarall exited with code {ec}{ls}stdout:{ls}{so}stderr:{ls}{so}".format(
                ec=unrarall_res.returncode,
                ls=os.linesep,
                so=unrarall_res.stdout,
                se=unrarall_res.stderr,
            ))

    def __prune_chown_move(self, dl_temp, completed_location):
        """
            remove bad extensions, remove empty dirs
        """

        # get files with the bad extensions and remove
        remove_files = filter(
            lambda x: x.suffix.strip(".").lower() in self.delete_exts,
            Path(dl_temp).rglob('*'),
        )
        for badfile in remove_files:
            self.log.info("Deleting {}".format(badfile))
            os.remove(badfile)

        # check for remaining directories
        all_dirs = sorted(filter(
            lambda x: x.is_dir() == True,
            Path(dl_temp).rglob('*'),
        ), reverse=True)
        for this_dir in all_dirs:
            if not os.listdir(this_dir):
                self.log.info("Removing empty dir {}".format(this_dir))
            else:
                self.log.debug("Dir {} is not empty - leaving alone".format(this_dir))

        # get the final destination
        dest_dir = self.download_folders.get(completed_location, self.default_dl_loc)
        self.log.info("Directory will be moved to {}".format(dest_dir))

        # chown everything
        for f in map(str, Path(dl_temp).rglob('*')):
            self.__chown(f)

        # move everything into final destination
        for f in sorted(map(str, Path(dl_temp).rglob('*'))):
            if os.path.exists(f):
                new_path = os.path.join(
                    dest_dir,
                    os.path.relpath(f, start=dl_temp),
                )
                self.log.debug("Moving {s} to {d}".format(s=f, d=new_path))
                shutil.move(f, new_path)

    def run(self, remote_config_file="tdl_config.ini"):

        # make sure we have a connection
        if not self.sftp_client:
            self.__connect_to_remote()

        # get the remote info from the config file
        self.remote_directory = self.__read_file(remote_config_file, parse_ini=True)['POSTDOWNLOAD']['JSON_LOCATION']
        self.log.info("Remote json directory: {}".format(self.remote_directory))
        remote_file_list = self.sftp_client.listdir_iter(path=self.remote_directory)

        # keep only .new files
        remote_file_list = [x for x in remote_file_list if os.path.splitext(x.filename)[1] == self.incompl_file_ext]
        self.log.info("{c} files found in remote directory {d}".format(
            c=len(remote_file_list),
            d=self.remote_directory,
        ))

        # get an iterator for all files to work on.
        self.log.info("Reading Json Files")

        # get a CompletedDownload object for each
        file_queue = map(self.__queue_json_file, remote_file_list)

        for cdl in file_queue:
            with tempfile.TemporaryDirectory(prefix="tdlclient_", dir=self.temp_dir) as dl_temp:

                # make sure every object has a temp dir - we didn't know it until now
                cdl.set_temp_dir(dl_temp)

                for i in cdl.files:
                    self.__download_file(i.remote_fullpath, i.local_temp_path, checksum=i.hash)

                if self.unrarall_exe:
                    self.log.info("Running unrarall in directory: {}".format(dl_temp))
                    self.__unrarall(dl_temp)

                # process what is left
                self.__prune_chown_move(dl_temp, cdl.location)

                # rename remote json file to completed path.
                self.__mark_json_done(cdl.json_file_path)
