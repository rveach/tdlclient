#! /usr/bin/env python

import os

class SingleFile:

    def __make_parent_dirs(self):
        os.makedirs(os.path.dirname(self.local_temp_path), exist_ok=True)

    def set_temp_dir(self, dl_temp):
        self.local_temp_path = os.path.join(
            dl_temp,
            os.path.relpath(
                self.remote_fullpath,
                start=self.remote_dl_loc,
            ),
        )
        self.__make_parent_dirs()

    def __init__(self, file_info, remote_dl_loc):
        self.remote_fullpath = file_info['fullpath']
        self.hash = file_info['hash']
        self.dl_temp = None
        self.local_temp_path = None
        self.remote_dl_loc = remote_dl_loc

class CompletedDownload:

    def set_temp_dir(self, dl_temp):
        """ sets the local temporary directory """
        self.dl_temp = dl_temp
        [x.set_temp_dir(self.dl_temp) for x in self.files]

    def __init__(self, json_contents, json_file_path):
        
        self.location = json_contents['location']
        self.name = json_contents['name']
        self.percent_done = json_contents['percent_done']
        self.transmission_hash = json_contents['transmission_hash']
        self.transmission_id = json_contents['transmission_id']
        self.files = [SingleFile(x, self.location) for x in json_contents['files']]
        self.dl_temp = None
        self.json_file_path = json_file_path