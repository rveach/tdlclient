#! /usr/bin/env python

import argparse
import flockfile
import logging
import os
from tdlclient.tdl_client import TdlClient

if __name__ == '__main__':

    lock = flockfile.FlockFile("tdlclient")
    lock.lock()

    

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--config", dest="config", default="/etc/tdl-client-config.yaml",
        help="Path to the config file.", 
    )
    parser.add_argument(
        "-v", "--verbose", dest="verbose",
        action='count', default=0,
        help="increase verbosity",
    )
    args = parser.parse_args()

    logging.basicConfig(
        level={
            0: logging.WARNING,
            1: logging.INFO,
        }.get(args.verbose, logging.DEBUG)
    )

    t = TdlClient(args.config)
    t.run()

    lock.unlock()
