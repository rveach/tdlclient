from distutils.core import setup
setup(
  name = 'tdlclient',
  packages = ['tdlclient'],
  version = '0.2.1',
  description = 'Download tdl files',
  author = 'ryan',
  author_email = 'ryan@example.com',
  url = 'https://gitlab.com/rveach/tdlclient',
  download_url = 'https://gitlab.com/rveach/tdlclient/-/archive/master/tdlclient-master.tar.gz',
  keywords = ['transmission'], # arbitrary keywords
  classifiers = [],
  install_requires=[
    'flockfile>=0.1.2',
    'paramiko>=2.6.0',
    'pyyaml>=5.2',
  ],
  scripts=['run_tdlclient.py'],
)
